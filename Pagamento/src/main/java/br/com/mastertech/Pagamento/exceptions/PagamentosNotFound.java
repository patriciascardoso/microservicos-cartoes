package br.com.mastertech.Pagamento.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus (code = HttpStatus.NOT_FOUND, reason = "Este cartão não possui pagamentos")
public class PagamentosNotFound extends RuntimeException {
}
