package br.com.mastertech.Pagamento.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus (code = HttpStatus.UNPROCESSABLE_ENTITY, reason = "O cartão não esta ativo para criar pagamentos.")
public class CartaoInativeException extends RuntimeException {
}
