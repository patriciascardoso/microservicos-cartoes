package br.com.mastertech.Pagamento.controllers;

import br.com.mastertech.Pagamento.DTOs.CadastrarPagamentoDTO;
import br.com.mastertech.Pagamento.DTOs.PagamentoCadastradoDTO;
import br.com.mastertech.Pagamento.models.Pagamento;
import br.com.mastertech.Pagamento.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PagamentoCadastradoDTO criarPagamento(@RequestBody @Valid CadastrarPagamentoDTO cadastrarPagamentoDTO){
            return pagamentoService.criarPagamento(cadastrarPagamentoDTO);
    }

    @GetMapping("/{id_cartao}")
    public List<Pagamento> consultarPagamentosCartao(@PathVariable(name = "id_cartao") int idCartao){
        return pagamentoService.consultarPagamentosCartao(idCartao);
    }
}
