package br.com.mastertech.Pagamento.client;

public class Cartao {

    private int id;
    private boolean ativo;

    public Cartao() {
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
