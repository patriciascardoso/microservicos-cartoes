package br.com.mastertech.Pagamento.repositories;

import br.com.mastertech.Pagamento.models.Pagamento;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.Nullable;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {

    @Nullable
    public List<Pagamento> findByCartaoId(int idCartao);

    public void deleteByCartaoId(int idCartao);
}
