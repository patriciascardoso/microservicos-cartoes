package br.com.mastertech.Pagamento.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CARTAO", configuration = CartaoClientConfiguration.class)
public interface CartaoClient {

    @GetMapping("/cartao/id/{id}")
    Cartao getCartaoById(@PathVariable int id);
}
