package br.com.mastertech.Cartao.controller;

import br.com.mastertech.Cartao.DTOs.AtivarCartaoDTO;
import br.com.mastertech.Cartao.DTOs.CartaoCriadoDTO;
import br.com.mastertech.Cartao.DTOs.CartaoDTO;
import br.com.mastertech.Cartao.DTOs.ConsultaCartaoDTO;
import br.com.mastertech.Cartao.models.Cartao;
import br.com.mastertech.Cartao.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CartaoCriadoDTO cadastrarCartao(@RequestBody @Valid CartaoDTO cartaoDTO){
            return cartaoService.criarCartao(cartaoDTO);
    }

    @PatchMapping("/{numero}")
    public CartaoCriadoDTO alteraStatusCartao(@PathVariable(name = "numero") int numero, @RequestBody AtivarCartaoDTO cartao){
            return cartaoService.alteraStatusCartao(numero, cartao);
    }

    @GetMapping("/{numero}")
    public ConsultaCartaoDTO consultarCartao(@PathVariable(name = "numero") int numero){
            return cartaoService.consultarCartao(numero);
    }

    @GetMapping("/id/{id}")
    public Cartao buscarCartaoPorId(@PathVariable int id){
            return cartaoService.buscarCartaoPorId(id);
    }
}
