package br.com.mastertech.Cartao.client;

import feign.Feign;
import feign.RetryableException;
import io.github.resilience4j.feign.FeignDecorator;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CLIENTE" , configuration = ClienteClientConfiguration.class)
public interface ClienteClient {

    @GetMapping("cliente/{id}")
    Cliente getClienteById(@PathVariable int id);
}
