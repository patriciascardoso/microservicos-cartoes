package br.com.mastertech.Cliente.repositories;

import br.com.mastertech.Cliente.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
}
